var yandexCommand = {
  checkTitle: function () {
    return this.assert.title("Яндекс");
  },
  submit: function () {
    return this.waitForElementVisible("@submitButton").click("@submitButton");
  },
  openAvtodispetcher: function (string) {
    return this.waitForElementVisible(
      'a[href="https://www.avtodispetcher.ru/distance/"]'
    ).click('a[href="https://www.avtodispetcher.ru/distance/"]');
  },
};

module.exports = {
  url: "http://yandex.ru",
  commands: [yandexCommand],
  elements: {
    searchBar: {
      selector: "input[id=text]",
    },
    submitButton: {
      selector: "button[type=submit]",
    },
    searchResult: {
      selector: "ul[id=search-result]",
    },
  },
};
