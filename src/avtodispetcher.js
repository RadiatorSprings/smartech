var avtodispetcherCommand = {
  checkUrl: function () {
    return this.assert.urlContains(this.url);
  },
  checkTitle: function () {
    return this.assert.title("Расчет расстояний между городами");
  },
  checkH1: function (string, message) {
    return this.waitForElementPresent("@h1").assert.containsText(
      "@h1",
      string,
      message
    );
  },
  calculateStart: function () {
    return this.moveToElement("h2", 0, 0).click("@calculate");
  },
  calculateAdditional: function () {
    return this.moveToElement('div[id="mapWrapper"]', 0, 0).click("@calculate");
  },
  changeDirection: function () {
    return this.moveToElement('div[id="mapWrapper"]', 0, 0).click(
      "@changeDirection"
    );
  },
};

module.exports = {
  url: "https://www.avtodispetcher.ru/",
  commands: [avtodispetcherCommand],
  elements: {
    h1: {
      selector: "h1[id=pageTitle]",
    },
    fromField: {
      selector: "input[name=from]",
    },
    toField: {
      selector: "input[name=to]",
    },
    fuelField: {
      selector: "input[name=fc]",
    },
    priceField: {
      selector: "input[name=fp]",
    },
    distanseSpan: {
      selector: "span[id=totalDistance]",
    },
    priceInput: {
      selector: "#summaryContainer > form > p",
    },
    changeDirection: {
      selector: "a[id=triggerFormD]",
    },
    accrossCityField: {
      selector: "input[name=v]",
    },
    calculate: {
      selector: "#CalculatorForm > div.submit_button > input[type=submit]",
    },
  },
};
