describe("Scenario for automation", function () {
  test("Search site on yandex", function (browser) {
    const yandex = browser.windowMaximize().page.yandex();

    yandex
      .navigate()
      .checkTitle()
      .waitForElementVisible("@searchBar")
      .setValue("@searchBar", "расчет расстояний между городами")
      .submit()
      .waitForElementVisible("@searchResult")
      .openAvtodispetcher("avtodispetcher.ru");

    browser.windowHandles(function (result) {
      browser.switchWindow(result.value[1]);
    });

    const avtodispetcher = browser.page.avtodispetcher();
    avtodispetcher
      .checkUrl()
      .checkTitle()
      .checkH1(
        "Расстояние между городами",
        "Open https://www.avtodispetcher.ru/distance/"
      );
  });

  test("Search direction", function (browser) {
    const avtodispetcher = browser.page.avtodispetcher();
    avtodispetcher
      .checkUrl()
      .waitForElementVisible("@fromField")
      .setValue("@fromField", "Тула")
      .setValue("@toField", "Санкт-Петербург")
      .clearValue("@fuelField")
      .setValue("@fuelField", "9")
      .clearValue("@priceField")
      .setValue("@priceField", "46")
      .calculateStart()
      .checkH1("Расстояние от Тулы до Санкт-Петербурга", "Рассчитали маршрут")
      .assert.containsText("@distanseSpan", 897, "Рассчитали растояние")
      .assert.containsText("@priceInput", 3726, "Рассчитали стоимость");
  });

  test("Change direction", function (browser) {
    const avtodispetcher = browser.page.avtodispetcher();
    avtodispetcher
      .checkH1("Расстояние от Тулы до Санкт-Петербурга")
      .changeDirection()
      .waitForElementPresent("@accrossCityField")
      .setValue("@accrossCityField", "Великий Новгород")
      .calculateAdditional()
      .waitForElementPresent("@distanseSpan")
      .assert.containsText("@distanseSpan", 966, "Рассчитали растояние")
      .assert.containsText("@priceInput", 4002, "Рассчитали стоимость");
  });
});
