FROM node:14.5.0-alpine3.12

WORKDIR /test-project
RUN npm install nightwatch -g

COPY . /test-project

CMD nightwatch --config nightwatch.js