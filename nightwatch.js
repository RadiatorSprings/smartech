module.exports = {
  // An array of folders (excluding subfolders) where your tests are located;
  // if this is not specified, the test source must be passed as the second argument to the test runner.
  src_folders: ["tests"],
  page_objects_path: "src",
  webdriver: {
    start_process: false,
    port: 4444,
    host: "localhost",
    // server_path: "node_modules/.bin/chromedriver",
    cli_args: [
      // very verbose geckodriver logs
      // '-vv'
    ],
  },

  test_settings: {
    default: {
      desiredCapabilities: {
        browserName: "chrome",
        chromeOptions: {
          w3c: false,
          args: ["--no-sandbox", "--disable-dev-shm-usage"],
        },
      },
    },
  },
};
